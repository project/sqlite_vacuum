<?php

function sqlite_vacuum(): void {
  \Drupal::database()->query('vacuum;');
}

function sqlite_vacuum_supported(): bool {
  return \Drupal::database()->databaseType() === 'sqlite';
}

function sqlite_vacuum_interval(): int {
  // 10800s = 3h.
  return 10800;
}

/**
 * Implements hook_cron().
 */
function sqlite_vacuum_cron(): void {
  if (!sqlite_vacuum_supported()) {
    return;
  }
  $interval = sqlite_vacuum_interval();
  // Set interval to 0 to disable.
  if ($interval <= 0) {
    return;
  }

  $lastRun = \Drupal::state()->get('sqlite_vacuum:last_run', 0);
  if (\Drupal::time()->getRequestTime() - $lastRun >= $interval) {
    sqlite_vacuum();
    \Drupal::state()->set('sqlite_vacuum:last_run', \Drupal::time()->getRequestTime());
  }
}
