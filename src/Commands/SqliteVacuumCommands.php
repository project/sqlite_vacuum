<?php

namespace Drupal\sqlite_vacuum\Commands;

use Drush\Drush;
use Drush\Commands\DrushCommands;
use Drupal\Core\Database\Database;

class SqliteVacuumCommands extends DrushCommands {
  /**
   * Vacuum a sqlite database.
   *
   * @param string $target
   *   The target database.
   * @param string $key
   *   The key of the database connection.
   *
   * @command sqlite:vacuum
   * @aliases sqlite-vacuum
   * @usage sqlite:vacuum dbname
   *   vacuum the database named dbname.
   */
  public function vacuum($target = 'default', $key = NULL) {
    $connectipn = Database::getConnection($target, $key);
    if ($connectipn->databaseType() === 'sqlite') {
      $connectipn->query('vacuum;');
    } else {
      Drush::logger()->error(dt('sql:vacuum works only on sqlite databases.'));
    }
  }
}

